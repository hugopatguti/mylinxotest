//
//  MyAppTestScenario.swift
//  MyLinxoUTests
//
//  Created by Hugo on 14/10/2021.
//

enum MyAppTestScenario {
    case networkError
    case technicalError
    case success
}
