//
//  ServiceProviderTests.swift
//  Tests iOS
//
//  Created by Hugo on 10/10/2021.
//

import XCTest
import RxSwift

@testable import MyLinxoTest

class ServiceProviderTests: XCTestCase {
    let disposeBag = DisposeBag()
    
    private func createSUT() -> ServiceProvider {
        return ServiceProvider()
    }

    func test_gettingPhotosWithWrongUrl() {
        // Given
        let sut = createSUT()
        let wrongUrlAddress = ""
        // When
        sut.getData(urlString: wrongUrlAddress)
            .subscribe { _ in
                XCTFail("Not suppose to work")
            } onFailure: { error in
                // Then
                let error = error as? MyAppErrorType
                XCTAssertEqual(error, MyAppErrorType.networkError)
            }
            .disposed(by: disposeBag)
    }
    
    func test_gettingPhotosWithGoodUrl() {
        // Given
        let sut = createSUT()
        let goodUrlAddress = String(format: Constants.unSlashPhotoAddress, Constants.clientId)
        // When
        sut.getData(urlString: goodUrlAddress)
            .subscribe { photos in
                // Then
                XCTAssertNotNil(photos)
                XCTAssertGreaterThan(photos.count, 0)
            } onFailure: { _ in
                XCTFail("Not suppose to be here")
            }
            .disposed(by: disposeBag)
    }
}
