//
//  ServiceProviderMock.swift
//  MyLinxoUTests
//
//  Created by Hugo on 14/10/2021.
//

import UIKit
import RxSwift

class ServiceProviderMock: ServiceProviderProtocol {
    
    var scenario: MyAppTestScenario
    
    init(with scenario: MyAppTestScenario) {
        self.scenario = scenario
    }
    
    func getData(urlString: String) -> Single<[Photo]> {
        switch scenario {
        case .networkError:
            return Single.error(MyAppErrorType.networkError)
        case .technicalError:
            return Single.error(MyAppErrorType.technicalError)
        case .success:
            return Single.just([Photo]())
        }
    }
}
