//
//  AsyncQueueMock.swift
//  MyLinxoUTests
//
//  Created by Hugo on 14/10/2021.
//

import Foundation

public class AsyncQueueMock: AsyncQueueProtocol {
    public init() {}
    public func async(group: DispatchGroup?, qos: DispatchQoS, flags: DispatchWorkItemFlags, execute work: @escaping @convention(block) () -> Void) {
        work()
    }
}
