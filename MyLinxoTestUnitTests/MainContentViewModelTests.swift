//
//  MainContentViewModelTests.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 14/10/2021.
//

import XCTest
import RxSwift

@testable import MyLinxoTest

class MainContentViewModelTests: XCTestCase {
    let disposeBag = DisposeBag()
    
    private func createSUT(with scenario: MyAppTestScenario) -> MainContentViewModel {
        let sut = MainContentViewModel()
        let serviceProvider = ServiceProviderMock(with: scenario)
        sut.serviceProvider = serviceProvider
        sut.mainQueue = AsyncQueueMock()
        return sut
    }

    func test_technicalErrorWhenGettingPhotos() {
        // Given
        let sut = createSUT(with: .technicalError)
        let urlString = "http://somme.address"
        // When
        sut.getPhotos(urlAddress: urlString)
        // Then
        XCTAssertEqual(sut.serviceState, .technicalError)
    }
    
    func test_networkErrorWhenGettingPhotos() {
        // Given
        let sut = createSUT(with: .networkError)
        let urlString = "http://somme.address"
        // When
        sut.getPhotos(urlAddress: urlString)
        // Then
        XCTAssertEqual(sut.serviceState, .networkError)
    }
    
    func test_gettingPhotosSuccefully() {
        // Given
        let sut = createSUT(with: .success)
        let urlString = "http://somme.address"
        // When
        sut.getPhotos(urlAddress: urlString)
        // Then
        XCTAssertEqual(sut.serviceState, .ready)
        XCTAssertNotNil(sut.photosLibrary)
    }
}

