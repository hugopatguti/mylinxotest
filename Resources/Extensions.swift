//
//  Extensions.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 12/10/2021.
//

import SwiftUI

extension Color {
    static let foreground = Color("foreground")
    static let foreground1 = Color("foreground1")
    static let background = Color("background")
}
