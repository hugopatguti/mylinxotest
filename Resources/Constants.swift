//
//  Constants.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 10/10/2021.
//

import Foundation
import SwiftUI

struct Constants {
    // Service
    static let unSlashPhotoAddress = "https://api.unsplash.com/photos/random/?count=200&client_id=%@"
    static let clientId = "EQEmkHXjBl-j3NNfH9NcA21XKVxR_owcRbjliMcEG1k"
    
    // Text values
    static let userUnknown = "Auteur inconnu"
    static let oups = "Oooooups"
    static let technicalErrorSubtitle = "Une erreur technique s'est produite"
    static let networkErrorSubtitle = "L'application n'arrive pas à se connecter au réseau"
    static let retry = "Réessayer"
    static let author = "Auteur : "
    static let photoTechnicalDetails = "Détails Techiniques"
    static let make = "Make"
    static let model = "Model"
    static let exposureTime = "Exposure time"
    static let aperture = "Aperture"
    static let focalLength = "Focal length"
    static let iso = "Iso"
    static let plusToolBarText = "voir Plus"
    
    // Navigation titles
    static let navigationMainTitle = "Album"
    static let errorTitle = "Un petit problème"
    
    // Sizes
    static let margin: CGFloat = 8
    static let spaceBetweenElements = margin * 2
    static let defaultCornerRadius: CGFloat = 15
    static let thumbImageSize: CGFloat = 100
    static let regularImageSize: CGFloat = 300
    static let navigationBarHeight: CGFloat = 60
    static let rowHeight: CGFloat = 100
    static let scaleEffect: CGFloat = 3
    static let translucentOpacity: CGFloat = 0.5
    
    // Images
    static let errorPic = "exclamationmark.icloud"
    static let defautlImage = "photo"
    static let plusCircleImage = "plus.circle"
    static let closeImage = "xmark.circle"
}

enum MyAppStateType {
    case loading
    case networkError
    case ready
    case technicalError
    
    var subtitle: String {
        switch self {
        case .technicalError:
            return Constants.technicalErrorSubtitle
        case .networkError:
            return Constants.networkErrorSubtitle
        case .loading, .ready:
            return ""
        }
    }    
}

enum MyAppErrorType: Error {
    case networkError
    case technicalError
}
