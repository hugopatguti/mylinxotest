//
//  ServiceProviderProtocol.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 14/10/2021.
//

import RxSwift

protocol ServiceProviderProtocol {
    func getData(urlString: String) -> Single<[Photo]>
}
