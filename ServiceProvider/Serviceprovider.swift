//
//  Serviceprovider.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 14/10/2021.
//

import RxSwift
import Foundation

final class ServiceProvider: ServiceProviderProtocol {
    /// getData
    /// - Parameters:
    ///   - urlString: The string of URL address to get the photos
    /// - Returns: A flux of photos array
    func getData(urlString: String) -> Single<[Photo]> {
        return Single<[Photo]>.create { single in
            guard let url = URL(string: urlString) else { return Disposables.create {} }
            let session = URLSession(configuration: .default)
            session.dataTask(with: url, completionHandler: { data, _, error in
                guard let data = data else {
                    single(.failure(MyAppErrorType.networkError))
                    return
                }
                do {
                    let photosFromData = try JSONDecoder().decode([Photo].self, from: data)
                    single(.success(photosFromData))
                } catch {
                    single(.failure(MyAppErrorType.networkError))
                }
            }).resume()
            return Disposables.create()
        }
    }
}
