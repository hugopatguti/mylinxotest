//
//  MyLinxoTestApp.swift
//  Shared
//
//  Created by Hugo on 09/10/2021.
//

import SwiftUI

@main
struct MyLinxoTestApp: App {
    var body: some Scene {
        WindowGroup {
            MainContentView()
        }
    }
}
