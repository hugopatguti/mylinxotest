//
//  ImageUrls.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 12/10/2021.
//

struct ImageUrls: Codable, Hashable {
    var thumb: String?
    var regular: String?
}
