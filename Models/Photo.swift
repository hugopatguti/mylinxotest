//
//  Photo.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 10/10/2021.
//

import Foundation

struct Photo: Identifiable, Codable, Hashable {
    var id: String?
    var imageUrls: ImageUrls?
    var user: User
    var exif: Exif
    var description: String
    var author: String
    
    enum CodingKeys: String, CodingKey {
        case id, user, exif
        case description = "alt_description"
        case imageUrls = "urls"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        imageUrls = try values.decodeIfPresent(ImageUrls.self, forKey: .imageUrls)
        description = try values.decodeIfPresent(String.self, forKey: .description) ?? "Sans nom"
        user = try values.decodeIfPresent(User.self, forKey: .user) ?? User()
        exif = try values.decodeIfPresent(Exif.self, forKey: .exif) ?? Exif()
        author = "\(user.first_name ?? Constants.userUnknown) \(user.last_name ?? "")"
    }
}
