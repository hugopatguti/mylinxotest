//
//  Exif.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 10/10/2021.
//

import Foundation

struct Exif: Codable, Hashable {
    var make: String? = ""
    var model: String? = ""
    var exposure_time: String? = ""
    var aperture: String? = ""
    var focal_length: String? = ""
    var iso: Int? = 0
}
