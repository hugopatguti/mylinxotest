//
//  User.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 10/10/2021.
//

struct User: Codable, Hashable {
    var first_name: String?
    var last_name: String?
}
