//
//  MainContentViewModel.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 11/10/2021.
//

import Combine
import Foundation
import RxSwift

final class MainContentViewModel: ObservableObject {
    
    @Published var photosLibrary: [Photo] = []
    @Published var serviceState: MyAppStateType = .loading
    
    lazy var mainQueue: AsyncQueueProtocol = DispatchQueue.main
    lazy var serviceProvider: ServiceProviderProtocol = ServiceProvider()
    let disposeBag = DisposeBag()
    
    func viewDidAppear() {
        let urlAddress = String(format: Constants.unSlashPhotoAddress, Constants.clientId)
        getPhotos(urlAddress: urlAddress)
    }
    
    /// getPhotos
    /// - Parameter urlAddress: The string of an URL Address
    func getPhotos(urlAddress: String) {
        serviceProvider.getData(urlString: urlAddress)
            .subscribe { [weak self] photos in
                guard let self = self else { return }
                self.mainQueue.async(group: nil, qos: .unspecified, flags: []) { [weak self] in
                    self?.photosLibrary.append(contentsOf: photos)
                }
                self.setServiceState(with: .ready)
            } onFailure: { [weak self] error in
                guard let error = error as? MyAppErrorType else { return }
                self?.errorHandler(with: error)
            }
            .disposed(by: disposeBag)
    }
}

private extension MainContentViewModel {
    func setServiceState(with state: MyAppStateType) {
        mainQueue.async(group: nil, qos: .unspecified, flags: []) { [weak self] in
            self?.serviceState = state
        }
    }
    
    func errorHandler(with error: MyAppErrorType) {
        switch error {
        case .networkError:
            setServiceState(with: .networkError)
        case .technicalError:
            setServiceState(with: .technicalError)
        }
    }
}
