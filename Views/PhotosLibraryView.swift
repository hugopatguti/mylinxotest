//
//  PhotosLibraryView.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 11/10/2021.
//

import SwiftUI

struct PhotosLibraryView: View {
    @StateObject var viewModel: MainContentViewModel
    
    var body: some View {
        List {
            ForEach(viewModel.photosLibrary, id: \.self) { photo in
                NavigationLink(destination: PhotoDetailView(photo: photo)) {
                    PhotoCellView(photo: photo)
                }
                .onAppear {
                    if photo == viewModel.photosLibrary.last {
                        viewModel.viewDidAppear()
                    }
                }
            }
        }
        .environment(\.defaultMinListRowHeight, Constants.rowHeight)
    }
}
