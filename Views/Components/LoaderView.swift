//
//  LoaderView.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 11/10/2021.
//

import SwiftUI

struct LoaderView: View {
    var body: some View {
        ProgressView()
            .progressViewStyle(CircularProgressViewStyle(tint: .foreground))
            .scaleEffect(Constants.scaleEffect)
            .padding(.top, -Constants.navigationBarHeight)
    }
}

struct LoaderView_Previews: PreviewProvider {
    static var previews: some View {
        LoaderView()
    }
}
