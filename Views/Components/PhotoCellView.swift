//
//  PhotoCellView.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 11/10/2021.
//

import SwiftUI

struct PhotoCellView: View {
    let photo: Photo
    var body: some View {
        HStack() {
            if let thumbUrlImage = photo.imageUrls?.thumb {
                PhotoUrlImage(image: thumbUrlImage,
                              height: Constants.thumbImageSize,
                              width: Constants.thumbImageSize,
                              cornerRadius: Constants.defaultCornerRadius)
            } else {
                DefaultImage()
            }
            
            VStack(alignment: .leading, spacing: Constants.margin) {
                Text(photo.description)
                    .font(.title3)
                    .foregroundColor(.foreground)
                HStack {
                    Text(Constants.author)
                        .font(.footnote)
                        .bold()
                        .foregroundColor(.foreground1)
                    Text(photo.author)
                        .font(.footnote)
                        .foregroundColor(.foreground1)
                }
            }
        }
    }
}
