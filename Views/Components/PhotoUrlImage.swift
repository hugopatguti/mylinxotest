//
//  PhotoUrlImage.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 12/10/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct PhotoUrlImage: View {
    let image: String
    let height: CGFloat
    let width: CGFloat
    let cornerRadius: CGFloat
    
    var body: some View {
        WebImage(url: URL(string: image))
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: width, height: height)
            .clipped()
            .cornerRadius(cornerRadius)
    }
}
