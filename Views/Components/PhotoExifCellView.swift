//
//  PhotoExifCellView.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 13/10/2021.
//

import SwiftUI

struct PhotoExifCellView: View {
    let title: String
    let description: String
    var body: some View {
        HStack() {
            Text(title)
                .bold()
                .foregroundColor(.foreground)
            Spacer()
            Text(description)
                .foregroundColor(.foreground1)
        }
    }
}
