//
//  SwiftUIView.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 12/10/2021.
//

import SwiftUI

struct DefaultImage: View {
    var body: some View {
        Image(systemName: Constants.defautlImage)
            .scaledToFit()
            .foregroundColor(.foreground)
            .frame(width: Constants.thumbImageSize, height: Constants.thumbImageSize)
            .cornerRadius(Constants.defaultCornerRadius)
    }
}
