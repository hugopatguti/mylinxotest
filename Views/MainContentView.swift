//
//  MainContentView.swift
//  Shared
//
//  Created by Hugo on 09/10/2021.
//

import SwiftUI

struct MainContentView: View {
    @StateObject var viewModel = MainContentViewModel()
    
    init() {
        UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: UIColor.init(.foreground)]
    }
    var body: some View {
        NavigationView {
            switch viewModel.serviceState {
            case .loading:
                LoaderView()
            case .ready:
                PhotosLibraryView(viewModel: viewModel)
                    .navigationTitle(Constants.navigationMainTitle)
            case .networkError, .technicalError:
                ErrorView(errorSubtitle: viewModel.serviceState.subtitle, closure: {
                    viewModel.viewDidAppear()
                })
                    .navigationTitle(Constants.errorTitle)
            }
        }
        .accentColor(.foreground1)
        .onAppear(perform: viewModel.viewDidAppear)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainContentView()
    }
}
