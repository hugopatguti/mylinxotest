//
//  PhotoDetailView.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 13/10/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct PhotoDetailView: View {
    let photo: Photo
    @State var isTechnicalDetailsPresented = false
    var body: some View {
        ZStack(alignment: .bottom) {
            if let imageUrls = photo.imageUrls, let regularPhotoUrl = imageUrls.regular {
                WebImage(url: URL(string: regularPhotoUrl))
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .clipped()
                    .cornerRadius(Constants.defaultCornerRadius)
            }
            VStack() {
                Text(photo.description)
                    .frame(alignment: .center)
                    .font(.title)
                    .foregroundColor(.foreground)
                    .fixedSize(horizontal: false, vertical: true)
                Text(photo.author)
                    .font(.headline)
                    .foregroundColor(.foreground1)
            }
            .frame(width: UIScreen.main.bounds.size.width)
            .padding(.bottom, Constants.margin)
            .background(Color.background.opacity(Constants.translucentOpacity))
            .navigationBarTitleDisplayMode(.inline)
        }
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button {
                    self.isTechnicalDetailsPresented.toggle()
                } label: {
                    Label(Constants.plusToolBarText, systemImage: Constants.plusCircleImage)
                }
                
            }
        }
        .sheet(isPresented: $isTechnicalDetailsPresented) {
            TechnicalInformationView(photo: photo, isPresented: $isTechnicalDetailsPresented)
        }
    }
}
