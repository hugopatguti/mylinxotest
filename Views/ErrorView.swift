//
//  ErrorView.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 11/10/2021.
//

import SwiftUI

struct ErrorView: View {
    
    @State var errorSubtitle: String
    @State var closure: () -> Void
    
    var body: some View {
        
        VStack(alignment: .center) {
            Image(systemName: Constants.errorPic)
                .resizable()
                .frame(width: Constants.thumbImageSize, height: Constants.thumbImageSize, alignment: .center)
                .foregroundColor(.foreground)
                .padding(.bottom, Constants.spaceBetweenElements)
            Text(Constants.oups)
                .font(.title3)
                .foregroundColor(.foreground)
                .padding(.bottom, Constants.margin)
            Text(errorSubtitle)
                .font(.footnote)
                .foregroundColor(.foreground1)
                .padding(.bottom, Constants.margin)
            Button {
                closure()
            } label: {
                Text(Constants.retry)
            }
            .font(.body)
            .padding()
            .foregroundColor(.black)
            .background(Color.foreground)
            .cornerRadius(Constants.defaultCornerRadius)

        }
        .padding()
    }
}

struct ErrorView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ErrorView(errorSubtitle: Constants.technicalErrorSubtitle, closure: {})
        }
    }
}
