//
//  TechnicalInformationView.swift
//  MyLinxoTest (iOS)
//
//  Created by Hugo on 14/10/2021.
//

import SwiftUI

struct TechnicalInformationView: View {
    let photo: Photo
    @Binding var isPresented: Bool
    var body: some View {
        VStack {
            HStack{
                Spacer()
                Button {
                    isPresented.toggle()
                } label: {
                    Label("", systemImage: Constants.closeImage)
                }
                .foregroundColor(.foreground1)
            }
            List {
                Section(header: Text(Constants.photoTechnicalDetails)) {
                    PhotoExifCellView(title: Constants.make, description: photo.exif.make ?? "")
                    PhotoExifCellView(title: Constants.model, description: photo.exif.model ?? "")
                    PhotoExifCellView(title: Constants.exposureTime, description: photo.exif.exposure_time ?? "")
                    PhotoExifCellView(title: Constants.focalLength, description: photo.exif.focal_length ?? "")
                    PhotoExifCellView(title: Constants.iso, description: String(photo.exif.iso ?? 0))
                }
            }

        }
    }
}
