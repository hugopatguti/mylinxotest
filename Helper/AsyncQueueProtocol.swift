//
//  AsyncQueueProtocol.swift
//  MyLinxoTest
//
//  Created by Hugo on 14/10/2021.
//

import Foundation

public protocol AsyncQueueProtocol {
    func async(group: DispatchGroup?,
               qos: DispatchQoS,
               flags: DispatchWorkItemFlags,
               execute work: @escaping @convention(block) () -> Void)
}

extension DispatchQueue: AsyncQueueProtocol { }
